#!/usr/bin/env python3

from jinja2 import Environment, FileSystemLoader
from os import listdir
from os.path import isdir, join
import datetime
import sys

now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
env = Environment(loader=FileSystemLoader('/usr/local/bin/templates'))
workingdir = ""
template = ""
if len(sys.argv) > 2:
    workingdir = sys.argv[1]
    template = sys.argv[2]
if workingdir == "" or template == "":
    print("usage: %s <workingdir> <template>" % (sys.argv[0]))
    exit(1)

if isdir(workingdir):
    versions = [ f for f in listdir(workingdir) if isdir(join(workingdir, f)) and not f.startswith("_") ]
    versions.sort()
    template = env.get_template(template)
    indexHtml = template.render(versions=versions, lm=now)
    with open(join(workingdir, "index.html"), "wb") as fh:
        fh.write(indexHtml.encode('utf-8'))
else:
    print("[%s] is not a directory, skipping" % (workingdir))
