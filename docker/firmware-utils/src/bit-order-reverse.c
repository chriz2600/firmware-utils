#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "firmware-utils.h"

#define SPI_PAGE_SIZE 256

void initBuffer(uint8_t *buffer, int len) {
    int i;
    for (i = 0 ; i < len ; i++) {
        // flash default (unwritten) is 0xff
        buffer[i] = 0xff; 
    }
}

int main(int argc, char** argv) {
    unsigned char buffer[SPI_PAGE_SIZE];

    for(;;) {
        initBuffer(buffer, SPI_PAGE_SIZE);
        size_t bytes_read = fread(buffer, 1, SPI_PAGE_SIZE, stdin);
        if (bytes_read == 0)
            break;
        reverseBitOrder(buffer, SPI_PAGE_SIZE);
        fwrite(buffer, 1, SPI_PAGE_SIZE, stdout);
    }
}
